<?php

$lang = array(

'ajw_selecteditems_module_name' => 'Selected items',
'ajw_selecteditems_module_description' => 'Create arbitrary groups of items',

'ajw_selecteditems_selections' => 'Selections',
'ajw_selecteditems_create' => 'Create new selection',
'ajw_selecteditems_edit' => 'Select entries',

'ajw_selecteditems_id' => 'ID',
'ajw_selecteditems_title' => 'Title',
'ajw_selecteditems_name' => 'Name',
'ajw_selecteditems_description' => 'Description',
'ajw_selecteditems_no_items' => 'There are no selections.',
'ajw_selecteditems_delete' => 'Delete',
'ajw_selecteditems_items' => 'Items',
'ajw_selecteditems_available_items' => 'Available',
'ajw_selecteditems_selected_items' => 'Selected',

'ajw_selecteditems_selection_added' => 'Selection added.',
'ajw_selecteditems_selection_updated' => 'Selection updated.',
'ajw_selecteditems_delete_success' => 'Selection deleted.',

'ajw_selecteditems_delete_confirm' => 'Are your sure you want to delete this selection?',

'ajw_selecteditems_title_info' => '',
'ajw_selecteditems_name_info' => 'single word, no spaces',



'ajw_selecteditems_' => '',

''=>''
);

/* End of file lang.ajw_selecteditems.php */
/* Location: ./system/expressionengine/third_party/ajw_selecteditems/language/english/lang.ajw_selecteditems.php */
