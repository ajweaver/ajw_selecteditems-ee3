<?php

class Ajw_selecteditems_mcp {
	
	var $version = '3.0';
	
	var $module_name = "ajw_selecteditems";
	
	function __construct() {
	}
	
	/**
	 * Default controller to display list of saved selections
	 *
	 * @author Andrew Weaver
	 */
	function index() {
	
		// Load helpers
		ee()->load->library('table');
		ee()->load->helper('form');
		
		$data = array();
		$data["content"] = "index";
		
		ee()->db->where( "site_id", ee()->config->item('site_id') );
		$query = ee()->db->get( "exp_ajw_selecteditems" );
		$data["items"] = $query->result_array();
		
		$data["base_edit"] = ee('CP/URL', 'addons/settings/ajw_selecteditems/edit')->compile();
		$data["base_delete"] = ee('CP/URL', 'addons/settings/ajw_selecteditems/delete')->compile();
		
		$data["create_url"] = ee('CP/URL', 'addons/settings/ajw_selecteditems/edit')->compile();

		// return ee()->load->view('_wrapper', $data, TRUE);
		return array(
			'body' => ee()->load->view('_wrapper', $data, TRUE),
			'breadcrumb' => array( ee('CP/URL', 'addons/settings/ajw_selecteditems')->compile() => ee()->lang->line('ajw_selecteditems_module_name') ),
			'heading' => ee()->lang->line('ajw_selecteditems_module_name')
		);
	}

	/**
	 * Edit controller to create and edit selections
	 *
	 * @param string $id 
	 * @author Andrew Weaver
	 */
	function edit( $id = NULL ) {

		if( $id == NULL ) {
			$id = ee()->input->get("id");
		}

		// Load helpers
		ee()->load->library('table');
		ee()->load->helper('form');
		
		$data = array();
		$data["content"] = "edit";
		
		$data["selected_items"] = array();
		$data["filter"] = array();
		if( $id !== FALSE ) {
			ee()->db->where( "id", $id );
			$query = ee()->db->get( "exp_ajw_selecteditems" );
			$data["settings"] = $query->row_array();
			$data["filter"] = unserialize( $data["settings"]["filter"]);
			
			ee()->db->select( "entry_id, title" );
			ee()->db->where_in( "entry_id", explode("|", $data["settings"]["items"]) );
			$query = ee()->db->get( "exp_channel_titles" );
			$data["selected_items"] = $query->result_array();
		}

		$data["all_items"] = array();
		ee()->db->select( "entry_id, title" );
		$query = ee()->db->get( "exp_channel_titles" );
		$data["all_items"] = $query->result_array();
		
		$data["form_action"] = ee('CP/URL', 'addons/settings/ajw_selecteditems/update')->compile();
		
		$url = ee()->functions->fetch_site_index(0, 0) . QUERY_MARKER . 'ACT=' . ee()->cp->fetch_action_id('Ajw_selecteditems_mcp', 'refresh');

		ee()->javascript->output(
			array('
			
			function refresh() {
				$("#all").html("Loading...")
				$.ajax({
					type: "POST",
						url: "'.$url.'",
						data: $("#update_form").serialize(),
						success: function(data){
							$("#all").html(data)
					}
				});
			}
			
			var delay = (function(){
				var timer = 0;
				return function(callback, ms){
					clearTimeout (timer);
					timer = setTimeout(callback, ms);
				};
			})();
			
			$(function(){
				
				refresh();
				
				$("input[name=filter_keywords]").keyup(function() {
					delay(function(){
						refresh();
					}, 300 );
				});

				$("select").change( function() {
					refresh();
				})

				$("#items").hide();
				$("#all, #added").sortable({
					connectWith: ".dnd_list",
					placeholder: "highlight",
					update: function() {
						var order = $("#added").sortable("toArray");
						var added = new Array();
						for( o in order ) {
							added.push(order[o].substr(5));
						}
						$("#items").val(added.join("|"));
					}
				}).disableSelection();

				$("#refresh").click( function() {
					refresh();
				})

			});
			')
		); 
		ee()->javascript->compile();
		
		ee()->db->select( "channel_id, channel_title" );
		$query = ee()->db->get( "exp_channels" );
		$data["channels"] = array();
		$data["channels"][] = "All";
		foreach( $query->result_array() as $row ) {
			$data["channels"][ $row["channel_id"] ] = $row["channel_title"];
		}

		ee()->db->select( "cat_id, cat_name" );
		$query = ee()->db->get( "exp_categories" );
		$data["categories"] = array();
		$data["categories"][] = "All";
		foreach( $query->result_array() as $row ) {
			$data["categories"][ $row["cat_id"] ] = $row["cat_name"];
		}

		$data["order"] = array(
			"t.entry_id desc" => "Entry ID",
			"t.title asc" => "Title",
			"t.entry_date desc" => "Entry Date"
		);
		
		$data["limit"] = array(
			"25" => "25",
			"50" => "50",
			"100" => "100",
			"250" => "250",
			"0" => "All",
		);
		
		return array(
			'body' => ee()->load->view('_wrapper', $data, TRUE),
			'breadcrumb' => array( ee('CP/URL', 'addons/settings/ajw_selecteditems')->compile() => ee()->lang->line('ajw_selecteditems_module_name') ),
			'heading' => ee()->lang->line('ajw_selecteditems_edit')
		);
	}
	
	/**
	 * Save selection to database
	 *
	 * @author Andrew Weaver
	 */
	function update() {
				
		ee()->load->library('form_validation');
		ee()->form_validation->set_error_delimiters('<p class="notice">', '</p>');
		ee()->form_validation->set_rules('title', lang('ajw_selecteditems_title'), 'required');
		ee()->form_validation->set_rules('name', lang('ajw_selecteditems_name'), 'required|alpha_dash');
		
		$filter_fields = array(
			"filter_keywords",
			"filter_channel",
			"filter_category",
			"filter_order",
			"filter_limit"
		);
		$filter = array();
		foreach( $filter_fields as $f ) {
			$filter[ $f ] = ee()->input->post( $f, "" );
		}
		
		$data = array(
			"title" => ee()->input->post("title"),
			"name" => ee()->input->post("name"),
			"description" => ee()->input->post("description"),
			"items" => ee()->input->post("items"),
			"filter" => serialize($filter)
		);

		if( ee()->input->post("id") === FALSE ) {

			if (ee()->form_validation->run() === FALSE) {
				return $this->edit();
			}

			ee()->db->insert('exp_ajw_selecteditems', $data);
			ee()->session->set_flashdata('message_success', lang("ajw_selecteditems_selection_added") );
			ee()->functions->redirect( ee('CP/URL', 'addons/settings/ajw_selecteditems')->compile() );

		} else {

			if (ee()->form_validation->run() === FALSE) {
				return $this->edit( ee()->input->post("id") );
			}

			ee()->db->where('id', ee()->input->post("id") );
			ee()->db->update('exp_ajw_selecteditems', $data);
			ee()->session->set_flashdata('message_success', lang("ajw_selecteditems_selection_updated") );
			ee()->functions->redirect( ee('CP/URL', 'addons/settings/ajw_selecteditems/edit')->compile() . AMP . 'id=' . ee()->input->post("id"));
			
		}
		
	}

	/**
	 * Confirm and delete selection
	 *
	 * @author Andrew Weaver
	 */
	function delete() {

		$vars = array();

		if( ee()->input->post('confirm') != 'confirm' ) {

			// Change this depending on required action

			$data["content"] = 'delete';

			$data["id"] = ee()->input->get('id');
			$data["delete_action"] = ee('CP/URL', 'addons/settings/ajw_selecteditems/delete')->compile();

			$data["confirm_message"] = ee()->lang->line('ajw_selecteditems_delete_confirm');

			return array(
				'body' => ee()->load->view('_wrapper', $data, TRUE),
				'breadcrumb' => array( ee('CP/URL', 'addons/settings/ajw_selecteditems')->compile() => ee()->lang->line('ajw_selecteditems_module_name') ),
				'heading' => ee()->lang->line('ajw_selecteditems_delete_confirm')
			);

		} else {

			$id = ee()->input->post('id');

			ee()->db->where( "id", ee()->input->post('id') );
			$query = ee()->db->delete( "exp_ajw_selecteditems" );
			
			$message = ee()->lang->line('ajw_selecteditems_delete_success');
			ee()->session->set_flashdata('message_success', $message);
			ee()->functions->redirect( ee('CP/URL', 'addons/settings/ajw_selecteditems')->compile() );
			
		}

	}

	/**
	 * AJAX-called method to fill available items select box
	 *
	 * @author Andrew Weaver
	 */
	function refresh() {

		$channel_id = ee()->input->post("filter_channel", "0");
		$category_id = ee()->input->post("filter_category", "0");
		$keywords = ee()->input->post("filter_keywords", "");
		$order = ee()->input->post("filter_order", "t.entry_id desc");
		$limit = ee()->input->post("filter_limit", "0");
		
		ee()->db->select( "t.entry_id, title" );
		ee()->db->from( "exp_channel_titles t" );
		ee()->db->join( "exp_category_posts p", "p.entry_id = t.entry_id", "left outer" );
		ee()->db->where_not_in( "t.entry_id", explode("|", ee()->input->post("items") ) );
		if( $keywords != "" ) {
			ee()->db->like( "t.title", $keywords );
		}
		if( $channel_id != 0 ) {
			ee()->db->where( "t.channel_id", $channel_id );
		}
		if( $category_id != 0 ) {
			ee()->db->where( "p.cat_id", $category_id );
		}
		ee()->db->group_by( "t.entry_id" );
		ee()->db->order_by( $order );
		if( $limit != 0 ) {
			ee()->db->limit( $limit );
		}
		$query = ee()->db->get();
		
		foreach( $query->result_array() as $row ) {
			print '<li class="item" id="item-'.$row["entry_id"].'">'.$row["title"].'</li>';
		}
		exit;
	}

}

/* End of file mcp.ajw_selecteditems.php */