<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! class_exists('Channel') )
{
	require_once( PATH_MOD.'channel/mod.channel.php' );
}

class Ajw_selecteditems extends Channel {

	var $return_data    = '';

	/**
	 * Tag to display list of selected items
	 *
	 * @author Andrew Weaver
	 */
	function __construct() {
		// Call parent class constructor
		parent::__construct();
	}
	
	function entries() {
		
		// Fetch parameters
		$id = ee()->TMPL->fetch_param('id'); 
		$name = ee()->TMPL->fetch_param('name'); 
		$exclude_items = ee()->TMPL->fetch_param('exclude_items', 'no'); 

		// todo: validate parameters

		// Fetch items from database
		ee()->db->select( "items" );
		ee()->db->where( "id", $id );
		ee()->db->or_where( "name", $name );
		$query = ee()->db->get( "exp_ajw_selecteditems" );

		// Run channel entries tag with corect parameters
		if ($query->num_rows() > 0) {
			
			$row = $query->row_array();
			if( $exclude_items != "yes" ) {
				// Add selected entries to fixed_order parameter
				ee()->TMPL->tagparams['fixed_order'] = $row["items"];
			} else {
				// Add selected entries to entry_id parameter, preceeded by 'not'
				ee()->TMPL->tagparams['entry_id'] = 'not ' . $row["items"];
			}

		} else {

			// No selected items so use non-existant entry zero
			ee()->TMPL->tagparams['fixed_order'] = '0';

		}

		ee()->TMPL->tagparams['dynamic'] = 'off';
		// print_r( ee()->TMPL->tagparams );

		$this->return_data = parent::entries();
		return $this->return_data;
	}

}

/* End of file mod.ajw_selecteditems.php */