<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ajw_selecteditems_upd { 

	var $version        = '3.0'; 

	function __construct( $switch = TRUE ) 
	{ 
	} 

	function install() 
	{
		ee()->load->dbforge();

		$data = array(
			'module_name' => 'Ajw_selecteditems' ,
			'module_version' => $this->version,
			'has_cp_backend' => 'y'
			);

		$fields = array(
			'id' => array(
				'type' => 'int',
				'constraint' => '6',
				'unsigned' => TRUE,
				'auto_increment'=> TRUE
			),
			'site_id' => array(
				'type' => 'int',
				'constraint' => '4',
				'unsigned' => TRUE,
				'null' => FALSE,
				'default' => '1'
			),
			'title' => array(
				'type' => 'varchar',
				'constraint' => '255',
				'null' => FALSE
			), 
			'name' => array(
				'type' => 'varchar',
				'constraint' => '255',
				'null' => FALSE
			), 
			'description' => array(
				'type' => 'text',
				'null' => FALSE
			), 
			'items' => array(
				'type' => 'text'
			), 
			'filter' => array(
				'type' => 'text'
			)
		);

		ee()->dbforge->add_field($fields);
		ee()->dbforge->add_key('id', TRUE);
		ee()->dbforge->create_table('ajw_selecteditems');

		ee()->db->insert('modules', $data); 

		$data = array(
			'class' => 'Ajw_selecteditems_mcp',
			'method' => 'refresh'
		);
		ee()->db->insert('actions', $data);

		return TRUE;
	}

	function uninstall() 
	{ 
		ee()->load->dbforge();

		ee()->db->select('module_id');
		$query = ee()->db->get_where('modules', array('module_name' => 'Ajw_selecteditems'));

		ee()->db->where('module_id', $query->row('module_id'));
		ee()->db->delete('module_member_groups');

		ee()->db->where('module_name', 'Ajw_selecteditems');
		ee()->db->delete('modules');

		ee()->db->where('class', 'Ajw_selecteditems');
		ee()->db->delete('actions');

		ee()->db->where('class', 'Ajw_selecteditems_mcp');
		ee()->db->delete('actions');

		ee()->dbforge->drop_table('ajw_selecteditems');

		return TRUE;
	}

	function update($current = '')
	{
		if ($current == $this->version) {
			return FALSE;
		}
		ee()->load->dbforge();

		if ( $current < "2.1" )  {

			// Add AJAX action
			$data = array(
				'class' => 'Ajw_selecteditems_mcp',
				'method' => 'refresh'
			);
			ee()->db->insert('actions', $data);
			
		} 

		if ( $current < "2.2" )  {

			ee()->load->dbforge();
			$fields = array(
				'title' => array(
					'type' => 'varchar',
					'constraint' => '255',
					'null' => FALSE
				)
			);
			ee()->dbforge->add_column('ajw_selecteditems', $fields, 'site_id');
			$fields = array(
				'filter' => array(
					'type' => 'text'
				),
			);
			ee()->dbforge->add_column('ajw_selecteditems', $fields);
			
			ee()->dbforge->drop_column('ajw_selecteditems', 'orderby');
			ee()->dbforge->drop_column('ajw_selecteditems', 'channels');
			$fields = array(
			'cache' => array(
				'name' => 'items',
				'type' => 'text',
				),
			);
			ee()->dbforge->modify_column('ajw_selecteditems', $fields);
		}
		
		return TRUE;
	}

}

/* End of file upd.ajw_selecteditems.php */