# README #

Selected Items is an add-on for ExpressionEngine 3/4.

### Installation ###

* Copy the `ajw_selecteditems` folder into your `system/user/addons` folder and install the add-on from the Control Panel.

### Creating a list ###

From the Selected Item's Control Panel page click on "Create new selection"

The Title and Description are designed to help you label the list in the back-end.

The Name is used in the template tag.

Items can be filtered using the pulldown menu and search fields, and items can be dragged and dropped from the Available column into the Selected column. Items in the Selected column can be re-ordered by dragging and dropping.

Click on "Submit" to save your list.

### Template tag ###

The template tag is 

	{exp:ajw_selecteditems:entries}

and can use all the same parameters as the Channel Entries tag.

	{exp:ajw_selecteditems:entries 
		name="test" 
		show_expired="yes" show_future="yes" 
		status="open|closed"}

		<li>{title}</li>

	{/exp:ajw_selecteditems:entries}

The tag uses the additional parameters:

Use the `name=` parameter to select the name of the Selected Items list to display. You can alternatively use the `id=` parameter.

You can also add `exclude_items="yes"` to display *all* the items that are *not* in the list. You can use the normal channel entries parameters to limit this, eg:

	{exp:ajw_selecteditems:entries 
		name="products" 
		exclude_items="yes"
		channel="products" 
		orderby="title" sort="asc" limit="10"}

		<li>{title}</li>

	{/exp:ajw_selecteditems:entries}




