<?php

$this->table->set_template( $cp_table_template );

$this->table->set_heading(
	array(
		lang("ajw_selecteditems_id"),
		lang("ajw_selecteditems_title"),
		lang("ajw_selecteditems_name"),
		lang("ajw_selecteditems_description"),
		""
	)
);

if( count( $items ) == 0 ) {
	$this->table->add_row(
		array(
			'colspan' => 5,
			'data' => lang("ajw_selecteditems_no_items")
		)
	);
}

foreach( $items as $item ) {

	$row = array();
	$row[] = $item["id"];
	$row[] = '<a href="' . $base_edit . AMP . 'id=' . $item["id"] . '" class="selection">' . $item["title"] . '</a>';
	$row[] = $item["name"];
	$row[] = $item["description"];
	$row[] = '<a href="' . $base_delete . AMP . 'id=' . $item["id"] . '" class="delete">' . lang("ajw_selecteditems_delete") . '</a>';
	
	$this->table->add_row(
		$row
	);
	
}

?>

<?php 
	echo $this->table->generate();
	echo $this->table->clear();
?>

<p>
	<a href="<?php echo $create_url; ?>" class="btn action">Create new selection</a>
</p>
