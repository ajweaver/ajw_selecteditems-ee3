<style>
.dnd_list_container {
	float: left;
	width: 45%;
	margin-right: 20px;
}
.dnd_list_container label {
	line-height: 24px;
}
.dnd_list {
	border: 1px solid #B6C0C2;
	height: 160px;
	overflow: auto;
	background: white;
	padding: 2px;
	margin-bottom: 4px;
	list-style-type: none;
}

.dnd_list li {
	padding: 4px 4px 4px 4px;
	cursor: auto;
	margin: 4px 0;
}

.dnd_list .highlight {
	color: grey;
	border: 1px dotted grey;
	background-color: white;
	height: 1em;
}
</style>

<?php $this->view($content); ?>