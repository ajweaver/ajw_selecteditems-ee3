<?php

return array(
      'author'      => 'Andrew Weaver',
      'author_url'  => 'http://brandnewbox.co.uk/',
      'name'        => 'AJW Selected Items',
      'description' => '',
      'version'     => '1.0.0',
      'namespace'   => 'Brandnewbox\AJWSelectedItems',
      'settings_exist' => TRUE
);